FROM ubuntu:bionic

ARG work_dir=/mnt/test
ARG app_dir=/mnt/app
ARG rep_dir=/mnt/report

RUN mkdir $work_dir ; mkdir $app_dir ; mkdir $rep_dir
WORKDIR $work_dir

RUN apt-get update \
  && apt-get install -y python3-pip python3-dev libgles2-mesa qt5-default software-properties-common wget \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip
RUN apt-get install -y openjdk-11-jdk
RUN apt-get install -y allure
COPY requirements.txt $work_dir/requirements.txt
RUN pip3 install -r $work_dir/requirements.txt \
  && rm $work_dir/requirements.txt
