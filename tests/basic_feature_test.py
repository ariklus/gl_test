import json
import re
import pytest
import allure
import pexpect
import requests
import copy
import random
import string
from .test_users import VALID_USER_1, USER_STATS

AUTH_KEY = 'Bearer a07e47269b7558c1c9e429488d814315b6b7cba49ccf5c210eabbb78adaedea3'
APP_ADDR = '/mnt/app'
MENU_PROMPT = "1: Get user info.*"
ID_PROMPT = 'Please input user id'
# It's better to store such data in config file or in Jenkins' keychain (in case of creds) but whatever


def create_user(connection, user_data):
    connection.sendline('2')
    for key in USER_STATS:
        connection.expect("Please input user")
        connection.sendline(user_data[key])
    connection.expect('1: Get user info')


@pytest.fixture()
def connection():
    connection = pexpect.spawn(APP_ADDR + '/usermanager.py')
    connection.expect(MENU_PROMPT)
    return connection


@allure.feature('Create')
def test_create(connection):
    tmp_user = copy.copy(VALID_USER_1)
    # Generate random email to avoid repetition
    tmp_user['email'] = ''.join(random.choices(string.ascii_lowercase + string.digits, k=15)) + '@mail.com'
    create_user(connection, tmp_user)
    user_id = re.search(r'.*User id is (\d+)', str(connection.before)).group(1)
    assert 'User successfully created.' in str(connection.before)
    # Check that created user is accessible  and data is correct (whitebox way not to depend on app being functional)
    response = requests.get(f'https://gorest.co.in/public-api/users/{user_id}')
    parsed_response = json.loads(response.text)
    assert parsed_response['code'] == 200
    user_data = json.loads(response.text)['data']
    for key in USER_STATS:
        assert user_data[key] == tmp_user[key]


@allure.feature('Create')
def test_duplicate_email(connection):
    # Create same user twice to get duplicate email error
    for i in range(2):
        create_user(connection, VALID_USER_1)
    assert 'has already been taken' in str(connection.before)


@allure.feature('Update')
@pytest.mark.parametrize("updated", [{'name': 'Bill'}, {'gender': "Male"}])
def test_update(connection, updated):
    tmp_user = copy.copy(VALID_USER_1)
    # Generate random email to avoid repetition
    tmp_user['email'] = ''.join(random.choices(string.ascii_lowercase + string.digits, k=15)) + '@mail.com'
    create_user(connection, tmp_user)
    user_id = re.search(r'.*User id is (\d+)', str(connection.before)).group(1)
    # Update user
    connection.sendline('3')
    connection.expect(ID_PROMPT)
    connection.sendline(user_id)

    for key in USER_STATS:
        connection.expect("Please input new user")
        if key in updated:
            connection.sendline(updated[key])
        else:
            connection.sendline('')
    connection.expect(MENU_PROMPT)
    assert 'User successfully updated' in str(connection.before)

    # Check if user was updated
    response = requests.get(f'https://gorest.co.in/public-api/users/{user_id}')
    parsed_response = json.loads(response.text)
    assert parsed_response['code'] == 200
    user_data = json.loads(response.text)['data']
    for key in USER_STATS:
        if key in updated:
            assert user_data[key] == updated[key]


@allure.feature('Update')
def test_update_not_found(connection):
    # Try deleting item to make sure it des not exist
    connection.sendline('4')
    connection.expect(ID_PROMPT)
    connection.sendline('42')
    connection.expect(MENU_PROMPT)
    # Update user
    updated = {'name': 'Bill', 'gender': "Male"}
    connection.sendline('3')
    connection.expect(ID_PROMPT)
    connection.sendline('42')

    for key in USER_STATS:
        connection.expect("Please input new user")
        if key in updated:
            connection.sendline(updated[key])
        else:
            connection.sendline('')
    connection.expect(MENU_PROMPT)
    assert "Could not update - user not found" in str(connection.before)


@allure.feature('Delete')
def test_delete(connection):
    tmp_user = copy.copy(VALID_USER_1)
    # Generate random email to avoid repetition
    tmp_user['email'] = ''.join(random.choices(string.ascii_lowercase + string.digits, k=15)) + '@mail.com'
    create_user(connection, tmp_user)
    user_id = re.search(r'.*User id is (\d+)', str(connection.before)).group(1)
    # delete that user
    connection.sendline('4')
    connection.expect(ID_PROMPT)
    connection.sendline(user_id)
    connection.expect('User deleted successfully')
    # Check that deleted user cannot be found anymore
    response = requests.get(f'https://gorest.co.in/public-api/users/{user_id}')
    parsed_response = json.loads(response.text)
    assert parsed_response['code'] == 404


@allure.feature('Delete')
def test_delete_not_found(connection):
    # Delete some id twice to make sure that user does not exist on second attempt
    for i in range(2):
        connection.sendline('4')
        connection.expect(ID_PROMPT)
        connection.sendline('42')
        connection.expect(MENU_PROMPT)
    assert 'No such user' in str(connection.before)


@pytest.mark.parametrize("test_input", ['exit', '-2', '3.66'])
def test_invalid_input(test_input, connection):
    connection.sendline(test_input)
    connection.expect("Please input valid option")
