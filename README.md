To run tests with Docker:
Run in root of test repo to build docker image:
docker build . -t tests 
Run to start tests
docker run -v "%APP_DIR%":/mnt/app/ -v "%TEST_DIR":/mnt/test -v "%%REPORT_DIR":/mnt/report -it tests pytest basic_feature_test.py --alluredir /mnt/report/