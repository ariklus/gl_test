#!/usr/bin/env python3
import requests
import json


AUTH_KEY = 'Bearer a07e47269b7558c1c9e429488d814315b6b7cba49ccf5c210eabbb78adaedea3'
# It's better to store such data in config file or in Jenkins' keychain


class UserManager:
    def __init__(self, auth_key):
        self.auth_key = auth_key

    def start(self):
        while True:
            print("""
            1: Get user info
            2: Create user
            3: Update user
            4: Delete user
            5: Exit""")

            option = input()

            while option not in ('1', '2', '3', '4', '5'):
                print("Please input valid option")
                option = input()

            option = int(option)
            if option == 1:
                self.get_user()
            elif option == 2:
                self.add_user()
            elif option == 3:
                self.update_user()
            elif option == 4:
                self.delete_user()
            elif option == 5:
                print('Exiting')
                exit(0)

    def get_user(self):
        print("Please input user id")
        uid = input()
        while not uid.isnumeric():
            print("Please input valid id")
            uid = input()
        response = requests.get(f'https://gorest.co.in/public-api/users/{uid}',
                                headers={'Authorization': self.auth_key})
        if response.status_code == 404:
            print('No such user')
        elif response.status_code == 200:
            print(f'User details:\n{response.text}')
        else:
            print(f'Something weird happened - response code {response.status_code}')

    def add_user(self):
        user_data = {}

        print("Please input user name")
        name = input()
        user_data['name'] = name

        print("Please input user email")
        email = input()
        user_data['email'] = email

        print("Please input user gender (Male/Female)")
        gender = input()
        while gender not in ("Male", "Female"):
            print('This application supports only two genders. Please select one (Male/Female)')
            gender = input()
        user_data['gender'] = gender

        print("Please input user status (Active/Inactive)")
        status = input()
        while status not in ("Active", "Inactive"):
            print('Please select valid status (Active/Inactive)')
            status = input()
        user_data['status'] = status

        response = requests.post(f'https://gorest.co.in/public-api/users/',
                                 headers={'Authorization': self.auth_key},
                                 data=user_data)
        if response.status_code == 200:
            parsed_response = json.loads(response.text)
            if parsed_response['code'] == 201:
                print(f"User successfully created.\nUser id is {parsed_response['data']['id']}")
            else:
                print(f"Creation failed - see details: {parsed_response['data']}")
        else:
            print(f'something went wrong: see response for details\n{response.text}')

    def update_user(self):
        user_data = {}
        print("Please input user id")
        uid = input()
        while not uid.isnumeric():
            print("Please input valid id")
            uid = input()
        print("Please input new user name or leave blank to leave as is")
        name = input()
        if name != '':
            user_data['name'] = name

        print("Please input new user email or leave blank to leave as is")
        email = input()
        if email != '':
            user_data['email'] = email

        print("Please input new user gender (Male/Female)or leave blank to leave as is")
        gender = input()
        while gender not in ("Male", "Female", ''):
            print('This application supports only two genders. Please select one (Male/Female) '
                  'or leave blank to leave as is')
            gender = input()
        if gender != '':
            user_data['gender'] = gender

        print("Please input new user status (Active/Inactive) or leave blank to leave as is")
        status = input()
        while status not in ("Active", "Inactive", ''):
            print('Please select valid status (Active/Inactive)')
            status = input()
        if status != '':
            user_data['status'] = status

        response = requests.put(f'https://gorest.co.in/public-api/users/{uid}',
                                 headers={'Authorization': self.auth_key},
                                 data=user_data)
        if response.status_code == 200:
            parsed_response = json.loads(response.text)
            if parsed_response['code'] == 200:
                print(f"User successfully updated.\nUser id is {parsed_response['data']['id']}")
            elif parsed_response['code'] == 404:
                print("Could not update - user not found")
            else:
                print(f"update failed - see details: {parsed_response['data']}")
        else:
            print(f'something went wrong: see response for details\n{response.text}')

    def delete_user(self):
        print("Please input user id")
        uid = input()
        while not uid.isnumeric():
            print("Please input valid id")
            uid = input()
        response = requests.delete(f'https://gorest.co.in/public-api/users/{uid}',
                                   headers={'Authorization': self.auth_key})
        if response.status_code == 200:
            parsed_response = json.loads(response.text)
            if parsed_response['code'] == 404:
                print('No such user')
            elif parsed_response['code'] == 204:
                print(f'User deleted successfully:\n{response.text}')
            else:
                print(parsed_response)
        else:
            print(f'Something weird happened - response code {response.status_code}')


manager = UserManager(AUTH_KEY)
manager.start()
